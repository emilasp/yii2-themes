Тема админки для Yii2
=============================

Базовый функционал для проектов. Компоненты, хелперы.

Installation
------------

Добавление нового модуля

Add to composer.json:
```json
"emilasp/yii2-core": "*"
```
AND
```json
"repositories": [
        {
            "type": "git",
            "url":  "https://bitbucket.org/emilasp/yii2-core.git"
        }
    ]
```

to the require section of your `composer.json` file.

Configuring
--------------------------

Добавляем :

```php
'modules' => [
        'core' => []
    ],
```
