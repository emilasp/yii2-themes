<?php
namespace emilasp\themes\bundles;

use yii\web\AssetBundle;

/**
 * Class ThemeMaterialzeDistAsset
 * @package emilasp\admintheme\bundles
 */
class ThemeMaterialzeDistAsset extends AssetBundle
{
    /*public $publishOptions = [
        'only' => [
            'fonts/*',
            'css/*',
            'js/*',
            'icons/*',
            'images/*',
        ],
        'except' => [
            '*.html',
        ]
    ];*/

    public $sourcePath = '@vendor/emilasp/yii2-themes/assets/vendor/materialize/dist/';
    public $css        = [
        'css/materialize.css',
        'css/style.css',
        'js/plugins/prism/prism.css',
        'js/plugins/perfect-scrollbar/perfect-scrollbar.css',
        'js/plugins/chartist-js/chartist.min.css',
        'css/theme.css',
    ];
    public $js         = [
        'js/materialize.js',
        'js/plugins/prism/prism.js',
        'js/plugins/perfect-scrollbar/perfect-scrollbar.min.js',
        'js/plugins/chartist-js/chartist.min.js',
        'js/plugins.js',
    ];

    public $depends    = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        //'emilasp\site\backend\assets\AppAsset',
    ];
}
