<?php
namespace emilasp\themes\bundles;

use yii\web\AssetBundle;

/**
 * Class ThemeWolfAsset
 * @package emilasp\admintheme\bundles
 */
class ThemeWolfAsset extends AssetBundle
{
    /*public $publishOptions = [
        'only' => [
            'fonts/*',
            'css/*',
            'js/*',
            'icons/*',
            'images/*',
        ],
        'except' => [
            '*.html',
        ]
    ];*/

    public $sourcePath = '@vendor/emilasp/yii2-admin-theme/assets/';
    public $css        = [
        'vendor/wolf-html/css/compiled/theme.css',
        'vendor/wolf-html/css/vendor/brankic.css',
        'vendor/wolf-html/css/vendor/ionicons.min.css',
        'vendor/wolf-html/css/vendor/jquery.dataTables.css',
        'css/theme.css',
    ];
    public $js         = [
        'vendor/wolf-html/js/theme.js',
        'vendor/wolf-html/js/vendor/jquery.cookie.js',
        'vendor/wolf-html/js/vendor/jquery.dataTables.min.js',
    ];
    public $depends    = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'emilasp\site\backend\assets\AppAsset',
    ];
}
