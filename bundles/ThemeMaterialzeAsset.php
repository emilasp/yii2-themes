<?php
namespace emilasp\themes\bundles;

use yii\web\AssetBundle;

/**
 * Class ThemeMaterialzeAsset
 * @package emilasp\admintheme\bundles
 */
class ThemeMaterialzeAsset extends AssetBundle
{

    public $sourcePath = '@vendor/emilasp/yii2-themes/assets/custom';
    public $css        = [
        'css/theme.css',
    ];
    public $js         = [];

    public $depends    = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'emilasp\themes\bundles\ThemeMaterialzeDistAsset',
    ];
}
