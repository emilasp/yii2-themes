<?php
namespace emilasp\themes\widgets\materialized\Tasks;

use yii;
use yii\base\Widget;

/**
 * Class TasksWidget
 * @package emilasp\themes\widgets\materialized\Tasks
 */
class TasksWidget extends Widget
{
    public function init()
    {
    }

    public function run()
    {
       $this->render('tasks');
    }
}
