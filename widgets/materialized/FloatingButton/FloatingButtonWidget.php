<?php
namespace emilasp\themes\widgets\materialized\FloatingButton;

use yii;
use yii\base\Widget;

/**
 * Class FloatingButtonWidget
 * @package emilasp\themes\widgets\materialized\FloatingButton
 */
class FloatingButtonWidget extends Widget
{
    public function init()
    {
    }

    public function run()
    {
       echo $this->render('buttons');
    }
}
