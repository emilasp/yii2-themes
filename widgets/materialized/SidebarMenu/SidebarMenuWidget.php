<?php
namespace emilasp\themes\widgets\materialized\SidebarMenu;

use yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\base\Widget;
use yii\helpers\Html;

/**
 * Class SidebarMenuWidget
 * @package emilasp\themes\widgets\materialized\SidebarMenu
 */
class SidebarMenuWidget extends Widget
{
    public $menuPath = '@app/config/menu/';
    public $menuName = 'menu-theme';
    public $menus    = [];

    private $action;
    private $controller;
    private $module;
    private $userId;

    public function init()
    {
        $this->registerJs();

        $this->action     = Yii::$app->controller->action->id;
        $this->controller = Yii::$app->controller->id;
        $this->module     = Yii::$app->controller->module->id;
        $this->userId     = (!Yii::$app->user->isGuest) ? Yii::$app->user->id : null;
    }

    public function run()
    {
        $items = include(Yii::getAlias($this->menuPath . $this->menuName . '.php'));
        $items = $this->removeByRight($items);
        $items = $this->setActiveParent($items);

        $menu = Html::beginTag('aside', ['id' => 'left-sidebar-nav']);
        $menu .= $this->generateMenu($items, [
            'class' => 'side-nav fixed leftside-navigation',
            'id' => 'slide-out'
        ]);
        $menu .= '<a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only darken-2"><i class="mdi-navigation-menu" ></i></a>';
        $menu .= Html::endTag('aside');

        echo $menu;
    }

    /**
     * @param $items
     * @param string $classUl
     *
     * @return string
     */
    private function generateMenu($items, $options = [])
    {
        $html = Html::beginTag('ul', $options);

        if ($options) {
            $html .= $this->render('profile-menu');
        }

        foreach ($items as $item) {
            $html .= $this->generateItem($item);
        }
        $html .= Html::endTag('ul');
        return $html;
    }

    /**
     * @param $item
     *
     * @return string
     */
    private function generateItem($item)
    {
        if (isset($item['visible']) && !$item['visible']) {
            return '';
        }

        if (!is_array($item)) {
            $html = $item;
        } else {
            $html = Html::beginTag('li');

            $active = (isset($item['active']) && $item['active']) ? 'active' : '';

            $sub = isset($item['items']);
            if ($sub) {
                $html .= Html::beginTag('ul', ['class' => 'collapsible collapsible-accordion']);
                $html .= Html::beginTag('li');

                if (!isset($item['options'])) {
                    $item['options'] = [];
                }

                $html .= Html::beginTag('a', ArrayHelper::merge(
                    $item['options'],
                    ['class' => $active . ' collapsible-header waves-effect waves-cyan']
                ));
                if (isset($item['icon'])) {
                    $html .= Html::tag('i', '', ['class' => $item['icon']]);
                }
                $html .= Html::beginTag('span');
                $html .= $item['label'];
                $html .= Html::endTag('span');
                $html .= Html::endTag('a');

            } else {
                if (!isset($item['options'])) {
                    $item['options'] = [];
                }

                $html .= Html::beginTag('a', ArrayHelper::merge(
                    $item['options'],
                    ['href' => Url::toRoute($item['url']), 'class' => $active]
                ));
                if (isset($item['icon'])) {
                    $html .= Html::tag('i', '', ['class' => $item['icon']]);
                }
                $html .= Html::beginTag('span');
                $html .= $item['label'];
                $html .= Html::endTag('span');
                $html .= Html::endTag('a');
            }

            if ($sub) {
                $html .= Html::beginTag('div', ['class' => 'collapsible-body']);
                $html .= $this->generateMenu($item['items']);
                $html .= Html::endTag('div');
                $html .= Html::endTag('li');
                $html .= Html::endTag('ul');
            }
            $html .= Html::endTag('li');
        }
        return $html;
    }

    /** Убираем из списка меню все элементы не проходящие по правам
     *
     * @param $items
     *
     * @return mixed
     */
    private function removeByRight($items)
    {
        foreach ($items as $index => $item) {
            if (is_array($item)) {
                $items[$index]['active'] = $this->isActive($items[$index]['url']);

                if (isset($item['items'])) {
                    $items[$index]['items'] = $this->removeByRight($item['items']);

                    if (count($items[$index]['items']) == 0) {
                        unset($items[$index]);
                    }
                } else {
                    if (isset($item['role'])) {
                        $isAllowRole = false;
                        foreach ((array)$item['role'] as $role) {
                            if (Yii::$app->user->can($role)) {
                                $isAllowRole = true;
                            }
                        }
                        if (!$isAllowRole) {
                            unset($items[$index]);
                        }
                    }

                    if (isset($item['rule']) && is_callable($item['rule'])) {
                        $rule = $item['rule'];
                        if ($rule() !== true) {
                            unset($items[$index]);
                        }
                    }
                }
            }
        }
        return $items;
    }

    private function setActiveParent($items)
    {
        foreach ($items as $index => $item) {
            if (isset($item['items'])) {
                $items[$index]['active'] = $this->isActiveParent($item['items']);


                foreach ($item['items'] as $index2 => $item2) {
                    if (isset($item2['items'])) {
                        $this->isActiveParent($item2['items']);
                        $items[$index]['items'][$index2]['active'] = $this->isActiveParent($item2['items']);
                    }
                }

            }
        }
        return $items;
    }


    /**
     * @param $items
     *
     * @return bool
     */
    private function isActiveParent($items)
    {
        foreach ($items as $item) {
            if (isset($item['items'])) {
                return $this->isActiveParent($item['items']);
            }

            if (isset($item['active']) && $item['active']) {
                return true;
            }
        }
        return false;
    }

    /** Устанавливаем активные пункты меню
     *
     * @param $route
     *
     * @return bool
     */
    private function isActive($route)
    {
        $route = $this->decompositionRoute($route);

        if (Yii::$app->controller->module->id === $route['module']) {
            if (Yii::$app->controller->id === $route['controller'] || $route['controller'] === null) {
                if (Yii::$app->controller->action->id === $route['action'] || !$route['action']) {
                    return true;
                }
            }
        }
        return false;
    }

    /** Получаем наименования модуля, контроллера и экшена
     *
     * @param $route
     *
     * @return array
     */
    private function decompositionRoute($route)
    {
        $data = [
            'module'     => null,
            'controller' => null,
            'action'     => null,
        ];

        if (strpos($route, '/') !== false) {
            $route = substr($route, 1);
        }
        $dataRoute = explode('/', $route);
        $count     = count($dataRoute);
        for ($i = 0; $i < $count; $i++) {
            switch ($i) {
                case 0:
                    $data['module'] = $dataRoute[$i];
                    break;
                case 1:
                    $data['controller'] = $dataRoute[$i];
                    break;
                case 2:
                    $data['action'] = $dataRoute[$i];
                    break;
            }
        }

        return $data;
    }


    /** Рекурсивно выставляем активные пункты меню
     *
     * @param $items
     *
     * @return mixed
     */
    /* private function setActiveItems( $items ){
         foreach( $items as $index=>$item ){
             if(is_array($item)){
                 if(isset($item['items'])){
                     $items[$index]['items'] = $this->setActiveItems($item['items']);
                 }else{
                     $action = Yii::$app->rights->getNameAction( $this->action, $this->controller, $this->module );

                     foreach($items[$index]['role'] as $role){
                         if( $action==$role ){
                             $items[$index]['active'] = true;
                             break;
                         }
                     }
                 }
             }
         }
         return $items;
     }*/


    private function registerJs()
    {
        $js =
            <<<JS
$(function() {

    $('.menu-section .active').next(".submenu").css('display', 'block');

    //var first = $('.menu-section .active').parent().parent();

  /*  var parent = $('.menu-section .active').closest('.submenu');
    if (parent.length) {
        parent.css('display', 'block');
    }
    var parent2 = $('.menu-section .active').closest('li a');
    if (parent2.length) {
        parent2.addClass('toggled');
    }
    var parent = parent.parent().closest('.submenu');
    if (parent.length) {
        parent.css('display', 'block');
    }*/
});
JS;

        $this->view->registerJs($js, yii\web\View::POS_READY);
    }
}
