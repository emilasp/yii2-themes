<?php
namespace emilasp\themes\widgets\materialized\Breadcrumbs;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class Breadcrumbs
 * @package emilasp\themes\widgets\materialized\Breadcrumbs
 */
class Breadcrumbs extends  \yii\widgets\Breadcrumbs
{
    public $tag = 'div';
    public $containerOptions = [];
    public $options = [];
    public $innerContainerOptions = [];
    public $itemTemplate = "{link}\n";
    public $activeItemTemplate = "<span class=\"breadcrumb active\">{link}</span>\n";

    /**
     * Initializes the widget.
     */
    public function init()
    {
        if (!isset($this->containerOptions['class'])) {
            Html::addCssClass($this->containerOptions, ['breadcrumbsContainer' => 'breadcrumbs']);
        }
        if (!isset($this->options['class'])) {
            Html::addCssClass($this->options, ['wrapper' => 'nav-wrapper']);
        }
        if ($this->innerContainerOptions !== false && !isset($this->innerContainerOptions['class'])) {
            Html::addCssClass($this->innerContainerOptions, ['innerContainer' => 'col s12']);
        }
    }


    /**
     * Renders the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        if (empty($this->links)) {
            return;
        }
        echo Html::beginTag('div', $this->containerOptions);
        echo Html::beginTag($this->tag, $this->options);
        if ($this->innerContainerOptions !== false) {
            $innerContainerTag = ArrayHelper::remove($this->innerContainerOptions, 'tag', 'div');
            echo Html::beginTag($innerContainerTag, $this->innerContainerOptions);
        }
        echo implode('', $this->prepareLinks());
        if ($this->innerContainerOptions !== false) {
            echo Html::endTag($innerContainerTag);
        }
        echo Html::endTag($this->tag);
        echo Html::endTag('div');
    }


    /**
     * Renders a single breadcrumb item.
     * @param array $link the link to be rendered. It must contain the "label" element. The "url" element is optional.
     * @param string $template the template to be used to rendered the link. The token "{link}" will be replaced by the link.
     * @return string the rendering result
     * @throws InvalidConfigException if `$link` does not have "label" element.
     */
    protected function renderItem($link, $template)
    {
        $encodeLabel = ArrayHelper::remove($link, 'encode', $this->encodeLabels);
        if (array_key_exists('label', $link)) {
            $label = $encodeLabel ? Html::encode($link['label']) : $link['label'];
        } else {
            throw new InvalidConfigException('The "label" element is required for each link.');
        }
        if (isset($link['template'])) {
            $template = $link['template'];
        }
        if (isset($link['url'])) {
            $options = $link;
            Html::addCssClass($options, ['link' => 'breadcrumb']);
            unset($options['template'], $options['label'], $options['url']);
            $link = Html::a($label, $link['url'], $options);
        } else {
            $link = $label;
        }
        return strtr($template, ['{link}' => $link]);
    }
    /**
     * Generates all breadcrumb links and sets active states.
     * @return array all processed items
     * @uses [[renderItem]]
     * @throws InvalidConfigException
     */
    protected function prepareLinks()
    {
        $links = [];
        if ($this->homeLink === null) {
            $links[] = $this->renderItem([
                'label' => Yii::t('yii', 'Home'),
                'url' => Yii::$app->homeUrl,
            ], $this->itemTemplate);
        } elseif ($this->homeLink !== false) {
            $links[] = $this->renderItem($this->homeLink, $this->itemTemplate);
        }
        foreach ($this->links as $link) {
            if (!is_array($link)) {
                $link = ['label' => $link];
            }
            $links[] = $this->renderItem($link, isset($link['url']) ? $this->itemTemplate : $this->activeItemTemplate);
        }
        return $links;
    }
}
