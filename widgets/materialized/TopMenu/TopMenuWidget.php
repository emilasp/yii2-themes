<?php
namespace emilasp\themes\widgets\materialized\TopMenu;

use yii;
use yii\base\Widget;

/**
 * Class TopMenuWidget
 * @package emilasp\themes\widgets\materialized\TopMenu
 */
class TopMenuWidget extends Widget
{
    public function init()
    {
    }

    public function run()
    {
       $this->render('top-menu');
    }
}
